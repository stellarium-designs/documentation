# [sd] Modkit Core // How to set up your avatar
This tutorial will guide you through the process of setting up your avatar for the first time, start to finish.

While not strictly necessary, we recommend keeping an organized archive of all your various avatar parts, clothing, etc. in their original unpacked form; this will make getting things set up much easier, especially if you'll be setting up multiple avatars.

### Setting up your folder structure
Start by making a folder for your avatar as a whole, then within that make another folder (named something like `!) Base`). This folder will contain anything your avatar is incomplete without, such as your skin, shape, body parts...

You will need to copy into this folder:
- the `BOM Base Bits` folder that came with the Modkit Core
- a shape (if you're using a Layercraft kit, it comes with a shape to start with for the head and body it's made for)
- any number of skin ("Universal") layers, optionally organized into their own subfolder
- your Kemono or Regalia body, depending which your kit/skin is for (the **item itself**, not the whole package!) *Note: Regalia support is not yet implemented.*
- any body pieces/enhancers for said body that you want to use (legs, chest, etc.)
- your Happy Paws foxcat or fox head (again, the **item itself**)
- the foxcat/fox cheek fluff (optional)
- the foxcat/fox eyelashes (optional)
- your choice of eyes (Oculux or otherwise)
- your ears, tail, wings etc.
- any other pieces you never want to be without (such as the AO you want to use, a collar, etc.)

Once you've gotten all your bits and bobs in place, right click the Base folder and select `Replace current outfit`.

(Outfits, accessories, etc. for a given avatar can of course be organized into their own subfolders within the main avatar folder.)

### Priming
For each of the following, right click the item in your inventory, selet `Edit`, and then drag the `Installer Bootstrap` into the contents tab of the build floater:
- body and any enhancers
- foxcat/fox head and cheek fluff (if included), but *not* the eyelashes!
- any ears/tail/wings/etc. not designed for use with Layercraft or the Modkit Core *(if you can't tell, the bootstrap won't break anything; it's just redundant if there are already updatable scripts inside.)*

If there's anything you don't want the applier to set to BoM, simply create a notecard (or any other type of item) named exactly `SetSpec` and drag it into the contents alongside the bootstrap. *(This is recommended for items like e\*K's Kemonie Beanz, for example; things that don't use the same texture layout as any of the things your skin/kit is made for.)*

Once you have all the relevant items bootstrapped, put on the Modkit Core Installer and hit the button!

### WIP
hi I wrote a lot of this while very tired  
I probably should be asleep right now :D
